FROM python:3.9-alpine
ENV PYTHONUNBUFFERED 1
RUN apk update && \
    apk add build-base gettext postgresql-dev postgresql-client python3-dev jpeg-dev zlib-dev tzdata

WORKDIR /code
COPY requirements.txt /code/
RUN pip install --upgrade pip && \
    pip install -r requirements.txt
COPY . /code/
