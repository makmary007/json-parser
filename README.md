Это универсальный парсер для API c json.

## Как использовать:

Необходимо скопировать .env.example в .env и заполнить необходимые параметры.
Далее собрать проект.

Команда запуска парсера: ```python manage.py parse```

Superuser:

    login: test
    password: test
