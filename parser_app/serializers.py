from django.utils import timezone
from rest_framework import serializers

from .fields import CustomDateTimeField


class DataSerializer(serializers.Serializer):
    code = serializers.CharField(max_length=200)
    name = serializers.CharField(max_length=2000)
    parentcode_id = serializers.IntegerField(required=False, source='parentcode')
    startdate = serializers.DateTimeField(
        format='%Y-%m-%d %H:%M:%S.%f',
        input_formats=['%Y-%m-%d %H:%M:%S.%f'],
    )
    enddate = CustomDateTimeField(
        input_formats=['%Y-%m-%d %H:%M:%S.%f', ''],
        allow_null=True,
        required=False
    )
    status = serializers.CharField(max_length=7)
    budgtypecode = serializers.CharField(max_length=2)


class BudgetSerializer(serializers.Serializer):
    data = DataSerializer(many=True)
