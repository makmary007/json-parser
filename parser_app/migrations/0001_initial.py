# Generated by Django 3.1.3 on 2020-11-23 14:36

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Budget',
            fields=[
                ('code', models.CharField(max_length=8, primary_key=True, serialize=False, verbose_name='Код')),
                ('name', models.TextField(max_length=2000, verbose_name='Полное наименование')),
                ('startdate', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Дата начала действия записи')),
                ('enddate', models.DateTimeField(blank=True, default=django.utils.timezone.now, null=True, verbose_name='Дата окончания действия записи')),
                ('status', models.CharField(choices=[('ACTIVE', 'Актуальная запись'), ('ARCHIVE', 'Архивная запись')], default='ACTIVE', max_length=7, verbose_name='Статус записи')),
                ('budgtypecode', models.CharField(choices=[('00', 'Прочие бюджеты'), ('01', 'Федеральный бюджет'), ('02', 'Бюджет субъекта РФ'), ('03', 'Бюджеты внутригородских МО г. Москвы и г. Санкт-Петербурга'), ('04', 'Бюджет городского округа'), ('05', 'Бюджет муниципального района'), ('06', 'Бюджет Пенсионного фонда РФ'), ('07', 'Бюджет ФСС РФ'), ('08', 'Бюджет ФФОМС'), ('09', 'Бюджет ТФОМС'), ('10', 'Бюджет поселения'), ('98', 'Распределяемый доход'), ('99', 'Доход организации (только для ПДИ)'), ('EM', '(Unknown)')], default='00', max_length=2, verbose_name='Тип бюджета')),
                ('parentcode', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='parser_app.budget', verbose_name='Вышестоящий бюджет')),
            ],
            options={
                'verbose_name': 'Справочник бюджетов',
                'verbose_name_plural': 'Справочники бюджетов',
            },
        ),
    ]
