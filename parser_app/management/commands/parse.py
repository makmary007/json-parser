from django.conf import settings
from django.core.management.base import BaseCommand

from parser_app.models import Budget
from parser_app.serializers import BudgetSerializer
from parser_app.parser import Parser


class Command(BaseCommand):
    help = 'Save json to model'

    def handle(self, *args, **options) -> None:
        parser = Parser(url=settings.PARSE_URL, serializer=BudgetSerializer,
                        model=Budget, identifier=settings.PARSE_IDENTIFIER)
        parser.create_or_update()
