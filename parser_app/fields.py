from datetime import datetime

from rest_framework import serializers


class CustomDateTimeField(serializers.DateTimeField):
    """
    Если пустая строка, передавать None
    А не подставлять самую первую дату
    """

    def to_internal_value(self, value) -> datetime or None:
        if value:
            return super().to_internal_value(value)
        return None
