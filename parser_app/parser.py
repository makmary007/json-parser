from collections import OrderedDict

import requests


class Parser:

    def __init__(self, url, serializer, model, identifier):
        self.url = url
        self.serializer = serializer
        self.model = model
        self.identifier = identifier

    def get_data(self) -> dict:
        req = requests.get(self.url)
        data = req.json()
        return data

    def parse(self) -> OrderedDict:
        serializer = self.serializer(data=self.get_data())
        serializer.is_valid(raise_exception=True)
        val_data = serializer.validated_data
        return val_data

    def create_or_update(self) -> None:
        val_data = self.parse()
        [self.model.objects.update_or_create(pk=val_data[self.identifier],
                                             defaults={**val_data}) for val_data in val_data['data']]
