OTHER = '00'
FEDERAL = '01'
SUBJECT = '02'
CAPITALS = '03'
CITY = '04'
MUNICIPAL = '05'
PENSION = '06'
FSS = '07'
FFOMS = '08'
TFOMS = '09'
LOCAL = '10'
__empty__ = 'EM'

#Есть ли 13 код в документации не описан, возможно есть и другие

DISTRIBUTED = '98'
ORGANIZATION = '99'

BUDGET_TYPE = [
    (OTHER, ('Прочие бюджеты')),
    (FEDERAL, ('Федеральный бюджет')),
    (SUBJECT, ('Бюджет субъекта РФ')),
    (CAPITALS, ('Бюджеты внутригородских МО г. Москвы и г. Санкт-Петербурга')),
    (CITY, ('Бюджет городского округа')),
    (MUNICIPAL, ('Бюджет муниципального района')),
    (PENSION, ('Бюджет Пенсионного фонда РФ')),
    (FSS, ('Бюджет ФСС РФ')),
    (FFOMS, ('Бюджет ФФОМС')),
    (TFOMS, ('Бюджет ТФОМС')),
    (LOCAL, ('Бюджет поселения')),
    (DISTRIBUTED, ('Распределяемый доход')),
    (ORGANIZATION, ('Доход организации (только для ПДИ)')),
    (__empty__, ('(Unknown)')),
]


ACTIVE = 'ACTIVE'
ARCHIVE = 'ARCHIVE'

KBK_STATUS = [
    (ACTIVE, ('Актуальная запись')),
    (ARCHIVE, ('Архивная запись')),
]
