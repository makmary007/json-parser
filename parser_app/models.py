from django.db import models
from django.utils import timezone

from .constants import ACTIVE, BUDGET_TYPE, KBK_STATUS, OTHER


class Budget(models.Model):
    code  = models.CharField('Код', max_length=8, primary_key=True)
    name = models.TextField('Полное наименование', max_length=2000)
    parentcode = models.ForeignKey('self', verbose_name='Вышестоящий бюджет',
                                   blank=True, null=True, on_delete=models.SET_NULL)
    startdate = models.DateTimeField('Дата начала действия записи', default=timezone.now)
    enddate = models.DateTimeField('Дата окончания действия записи',
                                   blank=True, null=True, default=timezone.now)
    status = models.CharField('Статус записи', max_length=7, choices=KBK_STATUS, default=ACTIVE)
    budgtypecode = models.CharField('Тип бюджета', max_length=2, choices=BUDGET_TYPE, default=OTHER)

    class Meta:
        verbose_name = 'Справочник бюджетов'
        verbose_name_plural = 'Справочники бюджетов'

    def __str__(self):
        return f'{self.code}: {self.name}'
